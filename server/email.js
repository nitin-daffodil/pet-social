Meteor.startup(function() {
	Kadira.connect('THWwk22tGLZZjW8fG', 'cbdb5a1b-8b1b-49a9-a704-82e34a1bc87d');
    // 1. Set up stmp
    //   your_server would be something like 'smtp.gmail.com'
    //   and your_port would be a number like 25

    process.env.MAIL_URL = 'smtp://' + SENDGRID_USERNAME + ':' + SENDGRID_PASSWORD + '@smtp.sendgrid.net:587';

    // 2. Format the email
    //-- Set the from address
    Accounts.emailTemplates.from = SENDGRID_SENDERID;

    
    //-- Application name
    Accounts.emailTemplates.siteName = 'Pet Pet Link';

    
    //-- Subject line of the email.
    Accounts.emailTemplates.verifyEmail.subject = function(user) {
        return 'Confirm Your Email Address for PPL';
    };

    
    //-- Email text    
    Accounts.emailTemplates.verifyEmail.text = function(user, url) {
        return 'Thank you for registering.  Please click on the following link to verify your email address: \r\n' + url;
    };
    
    // Accounts.emailTemplates.resetPassword.subject = function(user) {
    //     return user.username + ", following steps are used to reset the password"
    // };

    Accounts.urls.resetPassword = function(token) {
        console.log("I am called for resetPassword");
        return Meteor.absoluteUrl().replace("http:","http:")+"#/reset-password/" + token;
    };

    
    // Accounts.emailTemplates.resetPassword.html = function(user, url) {
    //     return  url;
    // };

    // 3.  Send email when account is created
    Accounts.config({
        sendVerificationEmail: true,
        loginExpirationInDays: 1
    });   
    //Copy all of the properties in the settings over to the emailTemplates
    /*_.extend(Accounts.emailTemplates, {
        "siteName": "My Fantastic Site",
        "from": "no-reply@example.com"
    });*/

    var loginAttemptVerifier = function(parameters) {
        if (parameters.user && parameters.user.emails && (parameters.user.emails.length > 0) && parameters.methodName == 'login') {
            // is the user an admin ?
            /*var adminUser = (parameters.user.username === "admin");
            if (adminUser) {
                return parameters.allowed;
            }*/
            // return true if verified email, false otherwise.
            var found = _.find(parameters.user.emails, function(thisEmail) {
                return thisEmail.verified;
            });
            if (!found) {
                throw new Meteor.Error(500, 'We sent you an email. Please verify your self');
            }
            return found && parameters.allowed;
        } else {
            console.log("user has no registered emails.");
            return true;
        }
    };
    Accounts.onCreateUser(function(options, user) {
        // options.isPrimaryUser ? user.isPrimaryUser = options.isPrimaryUser : ''
        user.isPrimaryUser = options.isPrimaryUser || ''
        options.roles ? options.roles.push(ROLES.Authenticated) : ''
        user.roles = options.roles || [ROLES.Authenticated]
        user.profile = options.profile || ''
        return user;
    });
    Accounts.validateLoginAttempt(loginAttemptVerifier);
    if (Meteor.users.find({
            "roles": ROLES.Admin
        }).count() === 0) {
        Fixtures.createUser();
    }
    Fixtures.createRoles();
})
