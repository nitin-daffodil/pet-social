var fs = Npm.require('fs');

Meteor.methods({
    /**
     * delete a user
     * 
     * @summary Meteor.methods wrapper for [deleteUser](#deleteUser).
     * @isMethod true           
     * @locus Anywhere
     * @param  {String} targetUserId _id of the user to be deleted.
     * @memberOf Methods
     */
    deleteUser: function(targetUserId) {
        this.unblock()
        var loggedInUser = Meteor.user()
        if (!loggedInUser ||
            !Roles.userIsInRole(loggedInUser, [ROLES.Admin])) {
            throw new Meteor.Error(403, "Access denied")
        }

        var user = Meteor.users.findOne({
            _id: targetUserId
        })
        if (user && user.isPrimaryUser) {
            throw new Meteor.Error(405, "The user account admin cannot be deleted.")
        }
        Meteor.users.remove({
            _id: targetUserId
        })
    },
    /**
     * 
     * @summary Increase the no of click-count on Post.
     * @isMethod true
     * @locus Anywhere
     * @param  {String} id _id of the Post whose click count needs tobe increased.
     * @memberOf Methods
     */
    addClickCount: function(id) {
        this.unblock();//Reduces wait time of method
        
        Post.update({
            _id: id
        }, {
            $inc: {
                click_count: 1
            }
        })
    },
    /**
     * @summary Make the Post as featured Post.
     * @isMethod true
     * @locus Anywhere
     * @param  {String} id _id of the Post to mark the Post featured.
     * @after Only accessable to admin or primary user.
     * @memberOf Methods
     */
    makePostfeatured: function(id) {
        this.unblock()
        var loggedInUser = Meteor.user()
        if (!loggedInUser ||
            !Roles.userIsInRole(loggedInUser, [ROLES.Admin])) {
            throw new Meteor.Error(403, "Access denied")
        }
        Post.update({
            _id: id
        }, {
            $set: {
                featured: true,
                featuredOn: new Date()
            }
        });
    },
    /**
     * @summary Make the Post as Unfeatured Post.
     * @isMethod true
     * @locus Anywhere
     * @param  {String} id _id of the Post to make the Post unfeatured.
     * @after Only accessable to admin or primary user.
     * @memberOf Methods
     */
    makePostunfeatured: function(id) {
        this.unblock()
        var loggedInUser = Meteor.user()
        if (!loggedInUser ||
            !Roles.userIsInRole(loggedInUser, [ROLES.Admin])) {
            throw new Meteor.Error(403, "Access denied")
        }
        Post.update({
            _id: id
        }, {
            $set: {
                featured: false
            }
        });
    },
    /**
     * Delete a post
     * @summary Meteor.methods wrapper for [deletePost](#deletePost).
     * @isMethod true
     * @locus Anywhere
     * @param  {String} id _id of the Post to be deleted.
     * @after Only accessable to admin or primary user.
     * @memberOf Methods
     */
    deletePost: function(id) {
        this.unblock()
        var loggedInUser = Meteor.user()
        if (!loggedInUser ||
            !Roles.userIsInRole(loggedInUser, [ROLES.Admin])) {
            throw new Meteor.Error(403, "Access denied")
        }
        Post.remove({
            _id: id
        })
    },
    /** 
     * @summary Change Role of a particular user.  
     * @isMethod true
     * @locus Anywhere
     * @param  {String} id _id of the user whose role needs to change.
     * @param  {Array} Array of roles which is assigned to a particular user.
     * @after Only accessable to admin or primary user.
     * @memberOf Methods
     */
    changeRole: function(id, roles) {
        var loggedInUser = Meteor.user()
        roles.push(ROLES.Authenticated)
        roles = _.uniq(roles);
        if (!loggedInUser ||
            !Roles.userIsInRole(loggedInUser, [ROLES.Admin])) {
            throw new Meteor.Error(403, "Access denied")
        }
        Roles.setUserRoles(id, roles);
    },
    /** 
     * @summary Block or Unblock the user.  
     * @isMethod true
     * @locus Anywhere
     * @param  {String} id _id of the user to block/unblock
     * @param  {Boolean} value If block the user value should be true. If unblock the user value should be false. 
     * @after Only accessable to admin or primary user.
     * @memberOf Methods
     */
    blockUnblockUser: function(id, value) {
        var loggedInUser = Meteor.user()
        if (!loggedInUser ||
            !Roles.userIsInRole(loggedInUser, [ROLES.Admin])) {
            throw new Meteor.Error(403, "Access denied")
        }
        Meteor.users.update({
            _id: id
        }, {
            $set: {
                isBlocked: value
            }
        })
    },
    /** 
     * @summary used by admin to edit profile of other user.  
     * @isMethod true
     * @locus Anywhere
     * @param  {String} id _id of the user to edit
     * @param  {Object}  profile. 
     * @after Only accessable to admin or primary user.
     * @memberOf Methods
     */
    editProfile: function(id, profile) {
        var loggedInUser = this.userId
        if (!loggedInUser ||
            !Roles.userIsInRole(loggedInUser, [ROLES.Admin])) {
            throw new Meteor.Error(403, "Access denied")
        }
        Meteor.users.update({
            _id: id
        }, {
            $set: 
                profile
            
        })
    },
    /** 
     * @summary own profile edit.  
     * @isMethod true
     * @locus Anywhere     
     * @param  {Object}  profile. 
     * @after Only accessable to admin or primary user.
     * @memberOf Methods
     */
    changeProfile: function(profile) {
        var loggedInUser = this.userId;
        if (!loggedInUser) {
            throw new Meteor.Error(403, "Access denied")
        }
        Meteor.users.update({
            _id: loggedInUser
        }, {
            $set: profile            
        })
    },
    /** 
     * @summary Upload the image on Server.  
     * @isMethod true
     * @locus Anywhere
     * @param  {String} fileName Name of the image to upload.
     * @param  {Blob} blob Content of the image in Base64 form.
     * @memberOf Methods 
     * @returns Returns the url Where image is uploaded.
     */
    uploadImage: function(fileName, blob) {
        var timestamp = Date.now();
        var theDir = process.env.PWD + '/.uploads/' + timestamp
        var imageDataUrl = blob;
        var dataUrlRegExp = /^data:image\/\w+;base64,/;
        var base64Data = imageDataUrl.replace(dataUrlRegExp, "");
        var imageBuffer = new Buffer(base64Data, "base64");
        fs.writeFile(theDir + fileName, imageBuffer, function(err) {
            if (err) {
                throw err;
            } else {
                var path = theDir + fileName;
            }
        });
        return '/.uploads/' + timestamp + fileName;
    },
    likePost: function(id) {
        if (!this.userId) {
            throw new Meteor.Error(403, "You Should Login to like the post");
        }
        Post.update({
            _id: id
        }, {
            $push: {
                likeBy: this.userId
            },
            $inc: {
                like_count: 1
            }
        })
    },
    removelikePost: function(id) {
        if (!this.userId) {
            throw new Meteor.Error(403, "You Should Login to remove like on the post");
        }
        Post.update({
            _id: id
        }, {
            $pull: {
                likeBy: this.userId
            },
            $inc: {
                like_count: -1
            }
        })
    },
    flagPost: function(id) {
        if (!this.userId) {
            throw new Meteor.Error(403, "You Should Login to like the post");
        }
        
        Post.update({
            _id: id
        }, {
            $push: {
                flagBy: this.userId
            }
        })
    },
    unflagPost: function(id) {
        if (!this.userId) {
            throw new Meteor.Error(403, "You Should Login to remove like on the post");
        }
        Post.update({
            _id: id
        }, {
            $pull: {
                flagBy: this.userId
            }
        })
    },
    likeComment: function(id) {
        if (!this.userId) {
            throw new Meteor.Error(403, "You Should Login to like the comment");
        }
        Comments.update({
            _id: id
        }, {
            $push: {
                likeBy: this.userId
            }
        })
    },
    removeLikeComment: function(id) {
        if (!this.userId) {
            throw new Meteor.Error(403, "You Should Login to remove like on the comment");
        }
        Comments.update({
            _id: id
        }, {
            $pull: {
                likeBy: this.userId
            }
        })
    },
    addReply: function(id, reply) {
        Comments.update({
            _id: id
        }, {
            $push: {
                replies: {
                    reply: reply
                }
            }
        })
    }

})
