Fixtures =  {      
    createUser: function() {
        id = Accounts.createUser({
            email: "monu.saini@daffodilsw.com",
            password: "hrhk",
            username: 'monu-daffodil',
            profile: {
                firstName: "Super Admin",                
            },
            isPrimaryUser: true,
            roles : [ROLES.Admin]
        });
        Accounts.sendVerificationEmail(id);
    },
    createRoles: function() {
        var roles = Roles.getAllRoles().fetch()
        _.each(USER_ROLES,function(role){
            if(!(_.findWhere(roles ,{name: role }))) {
                Roles.createRole(role);
            }
        })
    }
}
