Meteor.publish('usersAndRoles', function() {
    var user = Meteor.users.findOne({
        _id: this.userId
    });
    //check if the user have the permission to access all the user and roles
    if (user && (Roles.userIsInRole(user._id, [ROLES.Admin]) || user.isPrimaryUser)) {
        return [
            Meteor.users.find(),
            Roles.getAllRoles()
        ]
    } else {
        this.ready();
    }
});

Meteor.publish('authorDetail', function(id) {
    return Meteor.users.find({
        _id: id
    }, {
        fields: {
            profile: 1,
        }
    });

});


Meteor.publish('category', function() {
    return Category.find({})
});

// Meteor.publish('commentsCount', function(category, flagedFilter) {
//     var self = this;
//     var filter = {};
//     category && category.length > 0 ? filter.catType = category : '';
//     var user = Meteor.users.findOne({_id: this.userId})
//     if(flagedFilter && user && ( Roles.userIsInRole(user._id, [ROLES.Admin]) || user.isPrimaryUser ) ) {
//        filter["flagBy"] =   { $exists: true, $not: {$size: 0} }
//     }
//     Post.find(filter).forEach(function(post) {
//         var commentCount = Comments.find({
//             postId: post._id
//         }).count();
//         self.added('countsComment', post._id, {
//             count: commentCount
//         });
//     });
//     self.ready()
// })


Meteor.publishComposite('post', function(category, headerFilter, flagedFilter) {
    var filter = {};
    category && category.length > 0 ? filter.catType = category : '';
    var user = Meteor.users.findOne({
        _id: this.userId
    });
    if (flagedFilter && user && (Roles.userIsInRole(user._id, [ROLES.Admin]) || user.isPrimaryUser)) {
        filter["flagBy"] = {
            $exists: true,
            $not: {
                $size: 0
            }
        }
    }
    var sortFilter = {};
    if (headerFilter) {
        sortFilter = {
            sort: {}
        }
        if (headerFilter == 'latest') {
            sortFilter.sort = {
                createdAt: -1
            }
        } else if (headerFilter == 'oldest') {
            sortFilter.sort = {
                createdAt: 1
            }
        } else if (headerFilter == 'mostPet') {
            sortFilter.sort = {
                like_count: -1
            }
        } else if (headerFilter == 'mostClicked') {
            sortFilter.sort = {
                click_count: -1
            }
        } else if (headerFilter == 'mostCommented') {
            sortFilter.sort = {
                comment_count: -1
            }
        }
    }
    var self = this;
    return {
        find: function() {
            return Post.find(filter, sortFilter);
        },
        children: [{
            find: function(post) {
                    // Find post author. Even though we only want to return
                    // one record here, we use "find" instead of "findOne"
                    // since this function should return a cursor.
                    return Meteor.users.find({
                        _id: post.postedBy
                    }, {
                        fields: {
                            username: 1,
                            picture: 1,
                        }
                    });
                }
                /*}, {
                    find: function(post) {
                        // Find top two comments on post
                        return Comments.find({
                            postId: post._id
                        }, {
                            sort: {
                                score: -1
                            },
                            limit: 2
                        });
                    },
                    children: [{
                        find: function(comment, post) {
                            // Find user that authored comment.
                            return Meteor.users.find({
                                _id: comment.authorId
                            }, {
                                limit: 1,
                                fields: {
                                    profile: 1
                                }
                            });
                        }
                    }]*/
        }]
    }
});


Meteor.publishComposite('comments', function(postId, limit) {
    var l = limit || 5;
    return {
        find: function() {
            return Comments.find({
                postId: postId
            }, {
                sort: {
                    commentedOn: -1
                },
                limit: l
            });
        },
        children: [{
            find: function(comment) {
                // Find comment creator. Even though we only want to return
                // one record here, we use "find" instead of "findOne"
                // since this function should return a cursor.
                return Meteor.users.find({
                    _id: comment.creatorId
                }, {
                    fields: {
                        username: 1,
                        picture: 1,
                    }
                });
            }
        }, {
            find: function(comment) {
                var repliesArray = []
                if (comment && comment.replies && comment.replies.length > 0) {
                    repliesArray = comment.replies.map(function(a) {
                        return a.authorId
                    })
                }
                return Meteor.users.find({
                    _id: {
                        $in: repliesArray
                    }
                }, {
                    fields: {
                        username: 1,
                        picture: 1,
                    }
                });
            }
        }]
    }
});
