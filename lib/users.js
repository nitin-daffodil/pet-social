// var Schema = {};
// Schema.User = new SimpleSchema({
//     createdAt: {
//         type: Date,
//         optional: true,
//         defaultValue: Date.now()
//     },
//     services: {
//         type: Object,
//         optional: true
//     },
//     username: {
//         type: String,
//         optional: true 
//     },
//     "services.password": {
//         type: Object,
//         optional: true
//     },
//     "services.password.bcrypt": {
//         type: String,
//         label: "Password",
//         optional: true
//     },
//     "services.password.reset": {
//         type: Object,
//         optional: true
//     },
//     "services.password.reset.token": {
//         type: String,
//         optional: true
//     },
//     "services.password.reset.email": {
//         type: String,
//         optional: true
//     },
//     "services.password.reset.when": {
//         type: Date,
//         optional: true
//     },
//     "services.email": {
//         type: Object,
//         optional: true,
//         label: 'email'
//     },
//     "services.email.verificationTokens": {
//         type: Array,
//         optional: true
//     },
//     "services.email.verificationTokens.$": {
//         type: Object,
//         optional: true
//     },
//     "services.email.verificationTokens.$.address": {
//         type: String,
//         optional: true
//     },
//     "services.email.verificationTokens.$.token": {
//         type: String,
//         optional: true
//     },
//     "services.email.verificationTokens.$.when": {
//         type: Date,
//         optional: true
//     },
//     "services.resume": {
//         type: Object,
//         optional: true
//     },
//     "services.resume.loginTokens": {
//         type: [Object],
//         optional: true
//     },
//     "services.resume.loginTokens.$.when": {
//         type: Date,
//         optional: true
//     },
//     "services.resume.loginTokens.$.hashedToken": {
//         type: String,
//         optional: true
//     },
//     emails: {
//         type: [Object],
//         optional: true
//     },
//     "emails.$.address": {
//         type: String,
//         label: 'Email',
//         optional: true
//     },
//     "emails.$.verified": {
//         type: Boolean,
//         optional: true
//     },
//     profile: {
//         type: Object,
//         optional: true
//     },
//     "profile.firstName": {
//         type: String,
//         optional: true
//     },
//     "profile.lastName": {
//         type: String,
//         optional: true
//     },
//     "profile.experience": {
//         type: String,
//         optional: true
//     },
//     "profile.description": {
//         type: String,
//         optional: true
//     },
//     "profile.phone": {
//         type: String,
//         optional: true
//     },
//     "profile.address": {
//         type: String,
//         optional: true
//     },
//     "profile.image": {
//         type: String,
//         optional: true
//     },
//     roles: {
//         type: Array,
//         optional: true
//     },
//     'roles.$': {
//         type: String,
//         optional: true        
//     },
//     isPrimaryUser: {
//         type: Boolean,
//         optional: true
//     },
//     isBlocked: {
//         type: Boolean,
//         optional: true,
//         defaultValue: false
//     }
// });
// Meteor.users.attachSchema(Schema.User);