subscriptions = new SubsManager();//cache subscriptions on client side

Router.configure({
    loadingTemplate: 'loading',
    layoutTemplate: 'layout',
    notFoundTemplate: 'notFound'
});

Router.route('/', {
    name: 'home',
    title: 'Home',
    yieldTemplates: {
        'post_list': { to: 'lside' },
        'post_right': { to: 'rside' },
        'footer': { to: 'footer' }
    },
    onBeforeAction: function(){
         var self = this; 
         // if (Accounts._resetPasswordToken) { 
         //    Session.set('resetPasswordToken', Accounts._resetPasswordToken);
         //    Router.go('resetPassword');
         // }
         if(this.params.query && this.params.query.category) {
            Session.set('categoryFilter',this.params.query.category)
         } else {
            Session.set('categoryFilter',null)
         }
         if (Accounts._verifyEmailToken) {
            Accounts.verifyEmail(Accounts._verifyEmailToken, function(err) {
                  if (err != null) {
                    if (err.message = 'Verify email link expired [403]') {
                      var message ='Sorry this verification link has expired.';
                     toastr.error('',message);                          
                    }
                  } else {
                    var message = "Thank you! Your email address has been confirmed.";
                     toastr.success('', message);
                  }
            });
            self.next();
        } else {                     
            self.next();
        }
    },
    fastRender: true//improves initial load time of app

});

Router.route('/timeline/:id', {
    name: 'timeline',
    title: 'Timeline',
    yieldTemplates: {
        'timeline': { to: 'lside' },
        'post_right': { to: 'rside' },
        'footer': { to: 'footer' }
    },
    onBeforeAction: function() {
        this.next();
    },
    fastRender: true
    
});

Router.route('/addPost', {
    name: 'uploadPost',
    title: 'Add Post',
    yieldTemplates: {
        'uploadPost': { to: 'lside' },
        'post_right': { to: 'rside' },
        'footer': { to: 'footer' }
    },
    onBeforeAction: function() {
        if(!Meteor.userId()) {
           toastr.error('You have to sign in to add a new post.') 
           Router.go('home');           
        }
        this.next();
    },
    fastRender: true
     
});

Router.route('/addCategory', {
    name: 'uploadCategory',
    title: 'Add Category',
    yieldTemplates: {
        'uploadCategory': { to: 'lside' },
        'post_right': { to: 'rside' },
        'footer': { to: 'footer' }
    },
    onBeforeAction: function() {
        if(!Meteor.userId()) {
           toastr.error('You have to sign in to add a new post.') 
           Router.go('home');           
        }
        this.next();
    },
    fastRender: true

    /*onBeforeAction: function() {
        if(!Meteor.userId()) {
           alert('Please Login to add post') 
           Router.go('home');           
        }
        this.next();
    }  */    
});


Router.route('/post/:id', {
    name: 'postDetail',
    path: '/post/:id',
    title: 'Detail',
    yieldTemplates: {
        'post_detail': { to: 'lside' },
        'post_right': { to: 'rside' },
        'footer': { to: 'footer' }
    },
    fastRender: true

    // waitOn: function() {
    //     return [
    //         Meteor.subscribe('comments', this.params.id,Session.get('limitForComment'))
    //      ]
    // }   
});

Router.route('/register', {
    name: 'register',
    title: 'Register',
    yieldTemplates: {
        'common': { to: 'lside' },
        'register': { to: 'rside' },
        'footer': { to: 'footer' }
    },
        fastRender: true

});
Router.route('/login', {
    name: 'login',
    title: 'Login',
    yieldTemplates: {
        'common': { to: 'lside' },
        'login': { to: 'rside' },
        'footer': { to: 'footer' }
    },
    onBeforeAction: function() {
        if(Meteor.userId()) {           
           Router.go('home');           
        }
        this.next();
    },
    fastRender: true

});

Router.route('/forgot', {
    name: 'forgotPassword',
    title: 'ForgotPassword',
    yieldTemplates: {
        'common': { to: 'lside' },
        'forgotPassword': { to: 'rside' },
        'footer': { to: 'footer' }
    },
        fastRender: true

});

Router.route('/account', {
    name: 'account',
    title: 'Account',
    yieldTemplates: {
        'account': { to: 'rside' },
        'footer': { to: 'footer' }
    },
    onBeforeAction: function() {
        if(!Meteor.userId()) {
           // toastr.error('You have to sign in to See User Detail'); 
           Router.go('home');           
        }
        this.next();
    },
    fastRender: true

});

Router.route('/reset', {
    name: 'resetPassword',
    title: 'Reset Password',
    yieldTemplates: {
        'common': { to: 'lside' },
        'resetPassword': { to: 'rside' },
        'footer': { to: 'footer' }
    },
    onBeforeAction: function() {
        if(!Accounts._resetPasswordToken) {
            Router.go('home')
        }
        this.next();
    },
        fastRender: true

});

Router.route('/change-password', {
    name: 'changePassword',
    title: 'Change Password',
    yieldTemplates: {
        'common': { to: 'lside' },
        'changePassword': { to: 'rside' },        
        'footer': { to: 'footer' }
    }
    /*onBeforeAction: function(){
        
       this.next();
    }*/
});

Router.route('/manage-user', {
    name: 'manageUser',
    title: 'User Management',
    /*yieldTemplates: {
        requireAdminUser(this.userId): { to: 'lside' },
        'footer': { to: 'footer' }
    }*/
    /*onBeforeAction: function(pause) {
         var user = Meteor.user()
         this.render('footer', {
             to: 'footer'
         })
         if (user) {
             if (Roles.userIsInRole(user._id, ['admin'])) {
                 // return 
                console.log("Roles true");
                 this.render('users', {
                     to: 'lside'
                 });
                 this.next();
             } else {
                console.log("Roles false");
                 // return 
                 this.render('accessDenied', {
                     to: 'lside'
                 });
                 pause();
             }
         } else {
            
             console.log("User not found");
             this.render('notFound', {
                 to: 'lside'
             });
             pause();
         }
     },*/
     /*waitOn: function() {
         console.log("Within waiton");
         return [
             Meteor.subscribe('users')
         ]
     }*/

    action: function() {
        this.render(requireAdminUser(Meteor.user(),'users'), {
            to: 'lside'
        });
        this.render('footer', {
            to: 'footer'
        });
    },
        fastRender: true


});


Router.route('/manage-post', {
    name: 'managePost',
    title: 'Post Management',

    action: function() {
        this.render(requireAdminUser(Meteor.user(),'managePost'), {
            to: 'lside'
        });
        this.render('footer', {
            to: 'footer'
        });
    },
        fastRender: true


});
function requireAdminUser(user,temp) {
    if (user) {
        if (Roles.userIsInRole(user._id, [ROLES.Admin])) {
            return temp
        } else {
            return 'accessDenied'
        }
    } else {
        return 'notFound'
    }
};

Router.map(function() {
  this.route('serverFile', {
    where: 'server',
    path: /^\/.uploads\/(.*)$/,
    action: function() {
      var fs = Npm.require('fs');
      var filePath = process.env.PWD + '/.uploads/' + this.params;
      var data = fs.readFileSync(filePath);
      this.response.writeHead(200, {
        'Content-Type': 'image'
      });
      this.response.write(data);
      this.response.end();
    }
  });
});