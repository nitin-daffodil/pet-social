Category = new Mongo.Collection("category");
var Schemas = {};
Schemas.cat = new SimpleSchema({
    name: {
        type: String,
        max: 10,
        label: "Name",
        unique: true
    },
    picture: {
        type: String,
        label: 'Choose Image',        
    }
});

Category.attachSchema(Schemas.cat);
/*Category._ensureIndex({name: 1}, {unique: 1});*/
