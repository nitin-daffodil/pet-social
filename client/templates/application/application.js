UI.registerHelper("pageTitle", function(){
  var title;

  if(Router.current() && Router.current().route && Router.current().route.options && Router.current().route.options.title) {    
    title = Router.current().route.options.title;
  }  else if(Router.current() && Router.current().route && Router.current().route.options){
    // default to route name, doctored up
    
    title = Router.current().route.options.name
    .replace(/([A-Z])/g, ' $1')
    .replace(/^./, function(str){ return str.toUpperCase(); });
  }

  document.title = "Pet Laugh | " + title;
});


Template.registerHelper('formatTs', function(context) {
    if (!context instanceof Date) {
        context = new Date(context)
    }
    var json = {};    
    json.date = moment(context).format("D MMM YYYY");
    json.time = moment(context).format("h:mm a");
    return json
        // context.getFullYear() + '-' + ('00' + (context.getMonth() + 1)).substr(-2) + '-' + ('00' + context.getDate()).substr(-2) + ' ' + ('00' + context.getHours()).substr(-2) + ':' + ('00' + context.getMinutes()).substr(-2) + ':' + ('00' + context.getSeconds()).substr(-2);
})

UI.registerHelper('queryPathFor', function(routeName, query) {

    queryParams = 'category=' + query
    return Router.routes[routeName].path({}, {
        query: queryParams
    })
})
/**
 * @global
 * @name  isLoddedIn
 * @isHelper true
 * @summary Calls [Meteor.user()](#meteor_user). Use `{{#if isLoddedIn}}` to check whether the user is logged in.
 */
UI.registerHelper('isLoddedIn', function(routeName, query) {
     return Meteor.user();   
})

UI.registerHelper('formatText', function(string, length) {
     return string.length > length ? string.substring(0,length-3) + '...' : string
})

UI.registerHelper('shareOnFacebookLink', function() {
  return 'https://www.facebook.com/sharer/sharer.php?&u=' + window.location.href;
});

UI.registerHelper('shareOnTwitterLink', function() {
  return 'https://twitter.com/intent/tweet?url=' + window.location.href + '&text=' + document.title;
});

UI.registerHelper('shareOnGooglePlusLink', function() {
  return 'https://plus.google.com/share?url=' + window.location.href;
});

Template.registerHelper('arrayLength', function (arr) {
    return arr && arr.length ? arr.length : 0 
});