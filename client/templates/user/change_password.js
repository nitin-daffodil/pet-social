var password = new ReactiveVar(null);
var confirmPassword = new ReactiveVar(null);
Template.changePassword.popup = new ReactiveVar(null);
Template.changePassword.helpers({
    'isFieldOk': function() {
        return password.get() == confirmPassword.get() ? false : true
    },
    'popup': function() {
        return Template.changePassword.popup && Template.changePassword.popup.get()
    }
})
Template.changePassword.events({
    'input #confirmPassword': function(event) {
        event.preventDefault();
        confirmPassword.set(event.currentTarget.value)
    },
    'input #password': function(event) {
        event.preventDefault();
        password.set(event.currentTarget.value)
    },
    'submit #changePassword-form': function(event) {
        event.preventDefault();        
        Accounts.changePassword(event.currentTarget[0].value, event.currentTarget[1].value, function(error) {
            if (error) {
                console.log('Error', error);
                error.reason ? $(".alerta").empty().html('<div class="alert alert-danger alert-error"><a href="#" class="close" data-dismiss="alert">&times;</a> ' + error.reason + '.</div>')  : '';
            } else {
                Template.changePassword.popup.set(true);
                // Router.go('home')
                
                $('#changePassword-form').trigger("reset");        
            }
        })
    }
})