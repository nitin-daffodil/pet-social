var preview = new ReactiveVar(null);
var image = new ReactiveVar(null);
var sharePost = new ReactiveVar(null);


Template.timeline.onRendered(function() {
    var timeline = this;
    Session.set("active", 'uploads');
    timeline.autorun(function() {
        if (Session.get("active") == 'about') {            
            Meteor.subscribe("authorDetail", Router.current().params.id);
        }
    })
})

Template.timeline.helpers({
    user: function() {
        return Meteor.users.findOne({
            _id: Router.current().params.id
        });
    },
    'sharePost': function() {
        return sharePost.get() == this._id ? true : false;
    },
    absoluteURL: function() {
        return Meteor.absoluteUrl()
    },
    isMyTimeline: function() {
        return Router.current().params.id == Meteor.userId() ? true : false;
    },
    about: function() {
        return Meteor.users.findOne({
            _id: Router.current().params.id
        });
    },
    preview: function() {
        if (image.get())
            return image.get()
        else
            return '/images/user.png'
    },
    isLiked: function(likedBy) {
        return likedBy && likedBy.indexOf(Meteor.userId()) > -1 ? true : false;
    },
    commentCount: function(postID) {
        var commentCount = CountComment.findOne({
            _id: postID
        });
        return commentCount && commentCount.count ? commentCount.count : 0;
    },
    posts: function() {
        var results = Post.find({
            postedBy: Router.current().params.id
        }).fetch()
        return results;
    },
    active: function(val) {
        return Session.get("active") == val ? true : false;
    }

})

Template.timeline.events({
    'click .profile_text': function() {
        $("#user_image").click();
    },
    'click .sharePost': function(event) {
        if (sharePost.get()) {
            sharePost.set(undefined);
        } else {
            sharePost.set(this._id);
        }
    },
    'change #user_image': function(event) {
        FS.Utility.eachFile(event, function(file) {
            preview.set(file.name);
            var my_reader = new FileReader();            
            my_reader.onload = function(event) {
                var result = my_reader.result;
                // var buffer = new Uint8Array(result); // convert to binary
                document.getElementById("profile_image").src = result;
                image.set(result);
                Meteor.call("uploadImage", preview.get(), image.get(), function(err, result) {
                    if (err) {
                        alert(err.reason);
                        return
                    } else {
                        var user = {
                            "profile.image": result
                        };                        
                        Meteor.call("changeProfile", user, function(err, result) {
                            if (err) {
                                toastr.error(err.reason)
                            } else {
                                toastr.success('Changes saved')
                            }
                        })
                    }
                })
            }
            my_reader.readAsDataURL(file);
        });


    },
    'click .edit_div': function(event) {
        event.preventDefault();        
        Router.go("account");
    },
    'click #about': function(event) {
        event.preventDefault();
        Session.set("active", 'about');

    },
    'click #pets': function(event) {
        event.preventDefault();
        Session.set("active", 'pets');

    },
    'click #album': function(event) {
        event.preventDefault();
        Session.set("active", 'album');

    },
    'click #uploads': function(event) {
        event.preventDefault();
        Session.set("active", 'uploads');

    },
    'click .likePost': function(event) {
        if (Meteor.userId()) {
            Meteor.call('likePost', this._id, function(err) {
                if (err) {
                    console.log(err.reason);
                }
            })
        } else {
            toastr.error("You have to sign in to like the post.");
        }
    },
    'click .unlikePost': function(event) {
        if (Meteor.userId()) {
            Meteor.call('removelikePost', this._id, function(err) {
                if (err) {
                    console.log(err.message)
                }
            })
        } else {
            toastr.error("You have to sign in to remove like on this post.");
        }
    },
    'click .flagPost': function(event) {
        if (Meteor.userId()) {
            Meteor.call('flagPost', this._id, function(err) {
                if (err) {
                    console.log(err.reason);
                }
            })
        } else {
            toastr.error("You have to sign in to flag the post.");
        }
    },
    'click .unflagPost': function(event) {
        if (Meteor.userId()) {
            Meteor.call('unflagPost', this._id, function(err) {
                if (err) {
                    console.log(err.message)
                }
            })
        } else {
            toastr.error("You have to sign in to remove like on this post.");
        }
    },

})
