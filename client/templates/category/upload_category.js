var preview = new ReactiveVar(null);
var image = new ReactiveVar(null);
var loading = new ReactiveVar(false);

Template.uploadCategory.helpers({
    preview: function() {
        return preview.get()
    },
    loading: function() {
        return loading.get();
    }
})

Template.uploadCategory.events({
    'change #category_image_type': function(event) {
        FS.Utility.eachFile(event, function(file) {
            preview.set(file.name);
            var my_reader = new FileReader();
            my_reader.onload = function(event) {
                var result = my_reader.result
                var buffer = new Uint8Array(result) // convert to binary
                document.getElementById("category_image").src = result;
                image.set(result)

            }
            my_reader.readAsDataURL(file);
        })
    },
    'submit #category-form': function(event) {
        event.preventDefault();
        Meteor.call("uploadImage", preview.get(), image.get(), function(err, result) {
            if (err) {
                alert("Error", err);
                return
            } else {
                var json  = {};
                json.name = event.currentTarget[0].value;
                json.picture = result;
                loading.set(true);
                Category.insert(json,function(err,_id){
                	loading.set(false);
                    if(err && err.error == 409) {
                		$(".alerta").empty().html('<div class="alert alert-danger alert-error"><a href="#" class="close" data-dismiss="alert">&times;</a> ' + 'Category already exists' + '.</div>')                            
                	} else if(err) {
                		console.log('Error',err);
                		$(".alerta").empty().html('<div class="alert alert-danger alert-error"><a href="#" class="close" data-dismiss="alert">&times;</a> ' + err + '.</div>')                             
                	} else {
	        			$('#category-form').trigger("reset");
	        			preview.set(null);
						image.set(null);                		
                	}
                });
            }
        })
    }
})
