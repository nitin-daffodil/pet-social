Template.post_right.helpers({
	categories : function() {
		return Category.find({}).fetch()
	},
	featuredPosts: function() {
		return Post.find({featured: true}).fetch()
	}
})

