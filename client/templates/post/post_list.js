var sharePost = new ReactiveVar(null); 
var postCount = new ReactiveVar(null); 
var lastRefreshTime = new ReactiveVar(null)

Template.post_list.onCreated(function() {
    lastRefreshTime.set(new Date());    
})


Template.post_list.helpers({
    isLiked: function(likedBy) {
        return likedBy && likedBy.indexOf(Meteor.userId()) > -1 ? true : false;
    },
    commentCount: function(postID) {
        var count =  Post.findOne({ _id: postID });
        return  count && count.comments  && count.comments.length ? count.comments.length : 0;
    },
    sharePost: function() {
       return sharePost.get() == this._id ? true : false;
    },
    absoluteURL : function() {
        return Meteor.absoluteUrl()
    },
    count: function() {
        return postCount.get();
    },
    posts: function() {
        var headerFilter = Session.get('headerFilter');
        var sortFilter = {};
        if (headerFilter) {
            sortFilter = {
                sort: {}
            };
            if (headerFilter == 'latest') {
                sortFilter.sort = {
                    createdAt: -1
                }
            } else if (headerFilter == 'oldest') {
                sortFilter.sort = {
                    createdAt: 1
                }
            } else if (headerFilter == 'mostPet') {
                sortFilter.sort = {
                    like_count: -1,
                    createdAt: -1
                }
            } else if (headerFilter == 'mostClicked') {
                sortFilter.sort = {
                    click_count: -1,
                    createdAt: -1
                }
            } else if (headerFilter == 'mostCommented') {
                sortFilter.sort = {
                    comment_count: -1,
                    createdAt: -1
                }
            }
        } else {
            sortFilter.sort = {
                 createdAt: -1
            }
        }        
        return Post.find({createdAt: {$lte: lastRefreshTime.get()}}, sortFilter);
        /*postCount.set(results.count());
        results = results.fetch()
        if (headerFilter == 'mostCommented') {
            var post_ids = CountComment.find({}, { sort: { count: -1 } }).fetch().map(function(post) {
                return post._id
            })
            results = _.sortBy(results, function(thing) {
                return post_ids.indexOf(thing._id);
            });
            return results;
        } else {
            return results
        }*/
    },
    newPostUpdates: function() {
        return newPostUpdate();
    }
})

Template.post_list.events({
    'click .likePost': function(event) {
        if (Meteor.userId()) {
            Meteor.call('likePost', this._id, function(err) {
                if (err) {
                    console.log(err.reason);
                }
            })
        } else {
            toastr.error("You have to sign in to like the post.");
        }
    },
    'click .unlikePost': function(event) {
        if (Meteor.userId()) {
            Meteor.call('removelikePost', this._id, function(err) {
                if (err) {
                    console.log(err.message)
                }
            })
        } else {
            toastr.error("You have to sign in to remove like on this post.");
        }
    },
    'click .flagPost': function(event) {
        if (Meteor.userId()) {
            Meteor.call('flagPost', this._id, function(err) {
                if (err) {
                    console.log(err.reason);
                }
            })
        } else {
            toastr.error("You have to sign in to flag the post.");
        }
    },
    'click .unflagPost': function(event) {
        if (Meteor.userId()) {
            Meteor.call('unflagPost', this._id, function(err) {
                if (err) {
                    console.log(err.message)
                }
            })
        } else {
            toastr.error("You have to sign in to remove like on this post.");
        }
        
    },
    'click .sharePost': function(event) {
        if(sharePost.get()) {
          sharePost.set(undefined);
        } else {
            sharePost.set(this._id);
        }
    },
    'change .flagedPost': function(e) {
        Session.set("flagedPost", $('.flagedPost').prop("checked"));
    },
    'click .latest': function(event) {
        Session.set("headerFilter", 'latest')
    },
    'click .oldest': function(event) {
        Session.set("headerFilter", 'oldest')
    },
    'click .mostPet': function(event) {
        Session.set("headerFilter", 'mostPet')
    },
    'click .mostClicked': function(event) {
        Session.set("headerFilter", 'mostClicked')
    },
    'click .mostCommented': function(event) {
        Session.set("headerFilter", 'mostCommented')
    },
    'click .showNewPosts': function(event) {
        event.preventDefault()
        updatelastRefreshTime();
    }
})

newPostUpdate = function() {
    return Post.find({
            createdAt: {
                $gt: lastRefreshTime.get()
            }
        }).count();
}

updatelastRefreshTime = function() {
    lastRefreshTime.set(new Date()); 
}
