Template.login.events({
    'submit .login-form': function(event) {
        event.preventDefault();
        console.log("qwerty");
        Meteor.loginWithPassword(event.currentTarget[0].value, event.currentTarget[1].value, function(err) {
            if(err) {
            	console.log("Error", err)
                err.reason ? $(".alerta").empty().html('<div class="alert alert-danger alert-error"><a href="#" class="close" data-dismiss="alert">&times;</a> ' + err.reason + '.</div>') : ''           	               
            } else {
            	Router.go('home');
                $('#login-form').trigger("reset");
            }
        })
    }
})
