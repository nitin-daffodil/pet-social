var password = new ReactiveVar(null);
var confirmPassword = new ReactiveVar(null);
var popup = new ReactiveVar(null);

Template.resetPassword.helpers({
    'isFieldOk': function() {
        return password.get() == confirmPassword.get() ? false : true
    },
    'popup': function() {
        return popup.get();
    }
})
Template.resetPassword.events({
    'input #confirmPassword': function(event) {
        event.preventDefault();
        confirmPassword.set(event.currentTarget.value)
    },
    'input #password': function(event) {
        event.preventDefault();
        password.set(event.currentTarget.value)
    },
    'submit #reset-form': function(event) {
        event.preventDefault();        
        if (Session.get('resetPasswordToken')) {
            Accounts.resetPassword(
                Session.get('resetPasswordToken'),
                event.currentTarget[0].value,
                function(error) {
                    if (error) {
                        console.log('Error', error);
                        error.reason ? alert(error.reason) : '';
                    } else {
                        Session.set('resetPasswordToken', null);
                        popup.set(true);
                        Meteor.setTimeout(function() {
                            popup.set(false);
                            Accounts._resetPasswordToken = '';
                            Router.go('home');
                        }, 3000)
                    }
                })

        } else {
            console.log("Session token expired");
        }
        $('#reset-form').trigger("reset");
    }
})


Tracker.autorun(function() {
    if (Session.get('resetPasswordToken') != undefined && Session.get('resetPasswordToken') != '') {
        Router.go('resetPassword');
    }
})

